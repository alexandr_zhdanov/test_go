CREATE TABLE contacts
(
    id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
    name VARCHAR(255) NOT NULL,
    tel VARCHAR(255),
    email VARCHAR(254)
);