package main

import (
	"database/sql"

	"github.com/gin-gonic/gin"
	_ "github.com/mattn/go-sqlite3"
)

var router *gin.Engine
var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("sqlite3", "db/db.sqlite")
	if err != nil {
		panic(err)
	}

}
func main() {

	defer db.Close()

	router = gin.Default()
	router.Static("/src", "./src")
	router.GET("/", GetList)
	router.GET("/api/contact", GetContact)
	router.GET("/api/contactDel/:id", DelContact)
	router.GET("/api/contactUpd", UpdContact)
	router.GET("/api/contactCrt", CrtContact)
	router.Run()
}
