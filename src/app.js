'use strict';

new Vue({
    el: '#app',
    data: {
    urlAPI: "http://localhost:8080/api/contact",
    urlAPIDel: "http://localhost:8080/api/contactDel",
	urlAPIUpd: "http://localhost:8080/api/contactUpd",
	urlAPIcrt: "http://localhost:8080/api/contactCrt",
	chowTable: true,
	chowCreate: false,
	chowUpdate: false,
    listContact:[],
    listConTemp:[],
	filter:[],
	filter: "",
    conID: "",
	name: "",
	tel: "",
	email:"",
	updConId:"",
    },
	computed:{
		filteredList: function() {
            this.computedlistContact = this.listContact;
            if (this.filter) {
                this.computedlistContact = this.computedlistContact.filter(item => item.Name.toUpperCase().includes(this.filter.toUpperCase()));
                return this.computedlistContact;
            }
            return this.computedlistContact;
        }
	},
    methods: {
        getList: function () {
            self = this;
            self.listContact.length = 0;
            let req = new XMLHttpRequest();
            req.open('GET', this.urlAPI, true);
            req.onreadystatechange = function () {
                if (req.readyState === 4) {
                    if (req.status === 200) {
                        self.listConTemp = JSON.parse(req.responseText);
                        for (let i = 0; i < self.listConTemp.length; i++) {
                            self.listContact.push({
								ContactID: self.listConTemp[i].Id,
                                Name: self.listConTemp[i].Name,
                                Tel: self.listConTemp[i].Tel,
                                Email: self.listConTemp[i].Email
                            });
                        }
                    }
                }
            };
            req.send(null);
        },
        delContact:function(Id){
            self = this;
            self.conID = Id
            let req = new XMLHttpRequest();
            req.open('GET', self.urlAPIDel+"/"+self.conID, true);
            req.send();
			this.getList();
        },
		updContact:function(){
            self = this;
            self.conID = self.updConId;
            let req = new XMLHttpRequest();
            req.open('GET', self.urlAPIUpd+"?id="+self.conID+"&name="+self.name+"&tel="+self.tel+"&email="+self.email, true);
            req.send();
			this.chowTable = true;
			this.chowUpdate= false;
			this.getList();
			self.name = "";
			self.tel = "";
			self.email = "";
			self.updConId = "";
        },
		crtContact:function(){
            self = this;
            let req = new XMLHttpRequest();
            req.open('GET', self.urlAPIcrt+"?name="+self.name+"&tel="+self.tel+"&email="+self.email, true);
            req.send();
			this.chowTable = true;
			this.chowCreate= false;
			this.getList();
			self.name = "";
			self.tel = "";
			self.email = "";
        },
		showCreateCon:function(){
			this.chowTable = false;
			this.chowCreate= true;
		},
		showUpdateCon:function(Id, n, t, e){
			this.updConId = Id;
			this.name = n;
			this.tel = t;
			this.email = e;
			this.chowTable = false;
			this.chowUpdate= true;
		},
		Cansel:function(){
			this.chowTable = true;
			this.chowCreate = false;
			this.chowUpdate = false;
		}
    },
    beforeMount(){
        this.getList();
    }
});