package main

import(
	"strconv"
	"fmt"
	"net/http"
	"github.com/gin-gonic/gin"
)

type contacts struct {
	Id    int
	Name  string
	Tel   string
	Email string
}

func CrtContact(c *gin.Context) {
	Name := c.Query("name")
	Tel := c.Query("tel")
	Email := c.Query("email")
	_, err := db.Exec("INSERT INTO Contacts (Name, Tel, Email) VALUES ($1, $2, $3)", Name, Tel, Email)
	if err != nil {
		panic(err)
	}
}

func UpdContact(c *gin.Context) {
	ID, _ := strconv.Atoi(c.Query("id"))
	Name := c.Query("name")
	Tel := c.Query("tel")
	Email := c.Query("email")

	_, err := db.Exec("UPDATE Contacts SET Name=$1, Tel=$2, Email=$3 WHERE Id = $4", Name, Tel, Email, ID)
	if err != nil {
		panic(err)
	}
}

func DelContact(c *gin.Context) {
	ID, _ := strconv.Atoi(c.Param("id"))
	result, err := db.Exec("DELETE FROM Contacts WHERE Id = $1", ID)
	if err != nil {
		panic(err)
	}
	fmt.Println(result.RowsAffected())
}
func GetList(c *gin.Context) {
	c.File("tpl/index.html")
}
func GetContact(c *gin.Context) {
	listDev := getAllContacts()
	c.JSON(http.StatusOK, listDev)
}


// Return a list of all the articles
func getAllContacts() []contacts {

	rows, err := db.Query("SELECT Id, Name, Tel, Email FROM Contacts")
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	contact := []contacts{}

	for rows.Next() {
		p := contacts{}
		err := rows.Scan(&p.Id, &p.Name, &p.Tel, &p.Email)
		if err != nil {
			fmt.Println(err)
			continue
		}
		contact = append(contact, p)
	}
	return contact 
}