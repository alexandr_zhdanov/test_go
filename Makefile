main.exe : main.go handlers.go 
	go build -o main.exe handlers.go main.go
	go get github.com/mattn/go-sqlite3
	go get -u github.com/gin-gonic/gin
	./main.exe
clean : 
	rm main.exe 